#include "can_bus_handler.h"

// Standard Header Files
#include <stdio.h>
#include <string.h>

// User-Defined Header Files
#include "board_io.h"
#include "can_bus.h"
#include "compass.h"
#include "project.h"
#include "uds.h"
// Constants for CAN BUS
static const uint16_t CAN_BAUD_RATE = 100;
static const uint16_t CAN_TRANSMIT_QUEUE_SIZE = 100;
static const uint16_t CAN_RECEIVE_QUEUE_SIZE = 100;
static const can__num_e CAN_BUS = can1;

static dbc_BRIDGE_GPS_s detination_gps = {};

static void can_bus_handler__board_led_reset(void) {
  gpio__set(board_io__get_led0());
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
  gpio__set(board_io__get_led3());
}

void can_bus_handler__init(void) {
  can__init(CAN_BUS, CAN_BAUD_RATE, CAN_RECEIVE_QUEUE_SIZE, CAN_TRANSMIT_QUEUE_SIZE, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(CAN_BUS);
  can_bus_handler__board_led_reset();
}

void can_bus_handler__reset_if_bus_off(void) {
  if (can__is_bus_off(CAN_BUS)) {
    can__reset_bus(CAN_BUS);
    printf("buss off\n");
  }
}

void can_bus_handler__process_all_received_messages_in_1hz(void) {
  can__msg_t can_receive_msg = {};
  static const int num_services = 2;
  static const int uds_id_start = 400;
  uint8_t rx_data[num_services]
                 [8]; // for UDS in our case the data will never exceed 8 bytes or single frame transmission
  isotp_params__s rx_params[num_services];
  for (int i = 0; i < num_services; i++)
    isotp__init(&rx_params[i], &rx_data[i], 8);
  while (can__rx(CAN_BUS, &can_receive_msg, 0)) {
    const dbc_message_header_t header = {.message_id = can_receive_msg.msg_id,
                                         .message_dlc = can_receive_msg.frame_fields.data_len};
    if (dbc_decode_BRIDGE_GPS(&detination_gps, header, can_receive_msg.data.bytes)) {
      compass__set_destination_gps(&detination_gps);
      gpio__set(board_io__get_led0());
    } else if (can_receive_msg.msg_id >= uds_id_start &&
               can_receive_msg.msg_id < (num_services + uds_id_start)) { // UDS message range
      isotp_rx(&rx_params[can_receive_msg.msg_id - uds_id_start], can_receive_msg);
      if (rx_params[can_receive_msg.msg_id - uds_id_start].fc_info.fc_flag == COMPLETE) {
        uds_service_handler(&rx_params[can_receive_msg.msg_id - uds_id_start]);
        memset(rx_data[can_receive_msg.msg_id - uds_id_start], 0,
               sizeof(rx_data[can_receive_msg.msg_id - uds_id_start]));
        memset(&rx_params[can_receive_msg.msg_id - uds_id_start], 0,
               sizeof(rx_params[can_receive_msg.msg_id - uds_id_start]));
      }
    }
  }
}

void can_bus_handler__transmit_message_in_10hz(void) {
  dbc_BRIDGE_GPS_s current_gps_coordinates = compass__get_current_coordinates();
  dbc_GEO_DEBUG_s current_gps_location_coordinates = {};
  current_gps_location_coordinates.GEO_GPS_latitude = current_gps_coordinates.BRIDGE_GPS_latitude;
  current_gps_location_coordinates.GEO_GPS_longitude = current_gps_coordinates.BRIDGE_GPS_longitude;
  dbc_GEO_COMPASS_s current_and_heading_angle = compass__get_current_and_destination_heading();
  dbc_encode_and_send_GEO_COMPASS(NULL, &current_and_heading_angle);
  dbc_encode_and_send_GEO_DEBUG(NULL, &current_gps_location_coordinates);
}

bool dbc_send_can_message(void *argument, uint32_t message_id, const uint8_t bytes[8], uint8_t dlc) {
  can__msg_t send_msg = {};
  send_msg.msg_id = message_id;
  send_msg.frame_fields.data_len = dlc;
  // printf("Sent\n");
  memcpy(send_msg.data.bytes, bytes, dlc);
  return can__tx(CAN_BUS, &send_msg, 0);
}