#include "uds_services.h"
#include "compass.h"
#include "uds.h"

void uds_service__ecu_reset() {
  printf("rcvd\n");
  NVIC_SystemReset();
}

bool uds_service__get_data_by_id(uint8_t did, char *data, size_t data_size) {
  bool result = false;
  dbc_BRIDGE_GPS_s current_coordinates = {};
  switch (did) {
  case 0:
    current_coordinates = compass__get_current_coordinates();
    snprintf(data, data_size, "%f,%f", (double)current_coordinates.BRIDGE_GPS_longitude,
             (double)current_coordinates.BRIDGE_GPS_latitude);
    result = true;
    break;
  default:
    result = false;
  }
  return result;
  return true;
}