#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

void uds_service__ecu_reset();
bool uds_service__get_data_by_id(uint8_t did, char *data, size_t data_size);