#include "uds_services.h"
#include "motor_logic.h"
#include "uds.h"

void uds_service__ecu_reset() {
  printf("rcvd\n");
  NVIC_SystemReset();
}

bool uds_service__get_data_by_id(uint8_t did, char *data, size_t data_size) {
  float speed;
  bool result = false;
  switch (did) {
  case 0:
    speed = get_current_speed_mph();
    snprintf(data, data_size, "%.2f", (double)speed);
    result = true;
    break;
  case 1:
    snprintf(data, data_size, "%d", 78);
    result = true;
    break;
  default:
    result = false;
  }
  return result;
}