#include "uds_services.h"
#include "uds.h"
#include "ultrasonic_sensor_handler.h"

void uds_service__ecu_reset() {
  printf("rcvd\n");
  NVIC_SystemReset();
}

bool uds_service__get_data_by_id(uint8_t did, char *data, size_t data_size) {
  sensor_t sensor;
  bool result = false;
  switch (did) {
  case 0:
    ultrasonic_sensor_handler__get_all_sensor_values(&sensor);
    snprintf(data, data_size, "%d,%d", sensor.back, sensor.front);
    result = true;
    break;
  default:
    result = false;
  }
  return result;
  return true;
}