// isotp.h
#pragma once

#include "FreeRTOS.h"

#include "queue.h"
#include <can_bus.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef enum frame_type__s {
  SINGLE_FRAME,
  FIRST_FRAME,
  CONSECUTIVE_FRAME,
  FLOW_CONTROL_FRAME,
} frame_type__s;

typedef enum frame_status__s {
  CONTINUE,
  WAIT,
  ABORT,
  COMPLETE,
} frame_status__s;

typedef struct flow_control_s {
  frame_status__s fc_flag;
  uint8_t block_size;
  uint8_t sep_time;
} flow_control_s;

typedef struct isotp_params__s {
  uint16_t CAN_ID;
  uint8_t recieve_buffer_size;
  uint8_t *recieve_buffer;
  uint8_t data_size;
  flow_control_s fc_info;
  uint32_t recieve_buffer_offset;
  uint8_t frame_id;
  frame_type__s frame_type;
} isotp_params__s;

void isotp__init(isotp_params__s *message_param, void *rcv_data_ptr, size_t rcv_data_size_in_bytes);

bool isotp_tx(isotp_params__s *message_param, can__num_e can, void *data_ptr, size_t data_size, bool is_control_frame);
// check size of data
void isotp_rx(isotp_params__s *message_param, can__msg_t msg_recieved);