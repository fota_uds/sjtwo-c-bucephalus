#pragma once

#include "bridge_buffer.h"
#include "gpio.h"
#include "project.h"
#include "sl_string.h"
#include "uart.h"
#include "ultrasonic_sensor_handler.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

extern float debug_motor_speed;
extern float debug_motor_speed_pwm;
extern float debug_geo_compass_current_heading;
extern float debug_geo_compass_destination_heading;
extern float debug_geo_compass_distance;
extern int debug_steer_move_speed;
extern float latitude;
extern float longitude;
extern int debug_battery_percentage;
extern DRIVER_STEER_direction_e debug_steer_direction;

typedef struct {
  float latitude;
  float longitude;
  char latitude_direction;
  char longitude_direction;

} gps_data;

typedef struct {

  // user commands
  uint8_t car_action;
  uint8_t headlight_command;
  uint8_t test_button;

  // ultrasonic
  uint16_t ultrasonic_left;
  uint16_t ultrasonic_right;
  uint16_t ultrasonic_front;
  uint16_t ultrasonic_back;

  // motor speed
  float motor_speed;

  // steer speed
  int16_t steer_move_speed;
  DRIVER_STEER_direction_e steer_direction;

  // Geo Compass
  uint16_t compass_current_heading;
  uint16_t compass_desitination_heading;
  uint8_t compass_distance;

} debug_information;

void bridge_controller_handler__initialize_bluetooth_module(void);
void bridge_controller_handler__parse_gps_data(const char *input_buffer);
void bridge_controller_handler__get_data_from_uart(void);
void bridge_controller_handler__erase_substring_in_buffer(int start_index, int number_of_bytes);
void bridge_controller_handler__get_single_gps_message(char *line);
bool bridge_controller_handler__get_gps_message_from_buffer(char *temp_buffer);
void bridge_controller_handler__get_gps_coordinates();
void bridge_controller_handler__get_destination_coordinates();
bool bridge_controller_handler__buffer_has_message(void);
bool bridge_controller_handler__get_start_stop_condition(void);
bool bridge_controller_handler__get_headlight_control_signal(void);
bool bridge_controller_handler__get_test_button_control_signal(void);
void bridge_controller_handler__send_debug_info(void);
void bridge_controller_handler__send_ping_status(void);
