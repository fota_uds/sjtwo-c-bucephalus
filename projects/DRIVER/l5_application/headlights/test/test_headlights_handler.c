#include "unity.h"

#include "Mockgpio.h"
#include "project.h"

#include "headlights_handler.c"

void test_headlights_handler__initialize_car_lights(void) {
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 17, GPIO__FUNCITON_0_IO_PIN, headlights);
  gpio__set_as_output_Expect(headlights);

  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 18, GPIO__FUNCITON_0_IO_PIN, tail_lights);
  gpio__set_as_output_Expect(tail_lights);

  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 22, GPIO__FUNCITON_0_IO_PIN, left_blinker);
  gpio__set_as_output_Expect(left_blinker);

  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_0, 15, GPIO__FUNCITON_0_IO_PIN, right_blinker);
  gpio__set_as_output_Expect(right_blinker);
  headlights_handler__initialize_car_lights();
}

void test_headlights_handler__turn_headlights_on(void) {
  gpio__reset_Expect(headlights);
  headlights_handler__turn_headlights_on();
}

void test_headlights_handler__turn_headlights_off(void) {
  gpio__set_Expect(headlights);
  headlights_handler__turn_headlights_off();
}

void test_headlights_handler__set_driver_steer_direction_and_move_speed() {
  dbc_DRIVER_STEER_SPEED_s test_steer_info;

  test_steer_info.DRIVER_STEER_move_speed = 10;
  test_steer_info.DRIVER_STEER_direction = DRIVER_STEER_direction_HARD_LEFT;
  headlights_handler__set_driver_steer_direction_and_move_speed(test_steer_info);
  TEST_ASSERT_EQUAL_INT(10, driver_steer_move_speed);
  TEST_ASSERT_EQUAL_INT(-2, driver_steer_direction);

  test_steer_info.DRIVER_STEER_move_speed = 0;
  test_steer_info.DRIVER_STEER_direction = DRIVER_STEER_direction_STRAIGHT;
  headlights_handler__set_driver_steer_direction_and_move_speed(test_steer_info);
  TEST_ASSERT_EQUAL_INT(0, driver_steer_move_speed);
  TEST_ASSERT_EQUAL_INT(0, driver_steer_direction);

  test_steer_info.DRIVER_STEER_move_speed = -7;
  test_steer_info.DRIVER_STEER_direction = DRIVER_STEER_direction_SOFT_LEFT;
  headlights_handler__set_driver_steer_direction_and_move_speed(test_steer_info);
  TEST_ASSERT_EQUAL_INT(-7, driver_steer_move_speed);
  TEST_ASSERT_EQUAL_INT(-1, driver_steer_direction);

  test_steer_info.DRIVER_STEER_move_speed = 3;
  test_steer_info.DRIVER_STEER_direction = DRIVER_STEER_direction_HARD_RIGHT;
  headlights_handler__set_driver_steer_direction_and_move_speed(test_steer_info);
  TEST_ASSERT_EQUAL_INT(3, driver_steer_move_speed);
  TEST_ASSERT_EQUAL_INT(2, driver_steer_direction);
}

void test_headlights_handler__service_tail_lights(void) {
  driver_steer_move_speed = -10;
  gpio__reset_Expect(tail_lights);
  headlights_handler__service_tail_lights();

  driver_steer_move_speed = -3;
  gpio__reset_Expect(tail_lights);
  headlights_handler__service_tail_lights();

  driver_steer_move_speed = 0;
  gpio__reset_Expect(tail_lights);
  headlights_handler__service_tail_lights();

  driver_steer_move_speed = 7;
  gpio__set_Expect(tail_lights);
  headlights_handler__service_tail_lights();

  driver_steer_move_speed = 10;
  gpio__set_Expect(tail_lights);
  headlights_handler__service_tail_lights();
}

void test_headlights_handler__service_blinkers(void) {
  driver_steer_direction = -2;
  gpio__reset_Expect(left_blinker);
  gpio__set_Expect(right_blinker);
  headlights_handler__service_blinkers();

  driver_steer_direction = -1;
  gpio__reset_Expect(left_blinker);
  gpio__set_Expect(right_blinker);
  headlights_handler__service_blinkers();

  driver_steer_direction = 0;
  gpio__set_Expect(left_blinker);
  gpio__set_Expect(right_blinker);
  headlights_handler__service_blinkers();

  driver_steer_direction = 1;
  gpio__reset_Expect(right_blinker);
  gpio__set_Expect(left_blinker);
  headlights_handler__service_blinkers();

  driver_steer_direction = 2;
  gpio__reset_Expect(right_blinker);
  gpio__set_Expect(left_blinker);
  headlights_handler__service_blinkers();
}