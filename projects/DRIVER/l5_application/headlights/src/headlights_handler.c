#include "headlights_handler.h"
#include "gpio.h"

static gpio_s headlights, tail_lights, left_blinker, right_blinker;
static int driver_steer_direction = 0;
static int driver_steer_move_speed = 0;
static bool destination_reached = false;

void headlights_handler__set_destination_reached(bool did_it_reach) { destination_reached = did_it_reach; }

void headlights_handler__initialize_car_lights(void) {
  headlights = gpio__construct_with_function(GPIO__PORT_0, 17, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_as_output(headlights);

  tail_lights = gpio__construct_with_function(GPIO__PORT_0, 18, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_as_output(tail_lights);

  left_blinker = gpio__construct_with_function(GPIO__PORT_0, 22, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_as_output(left_blinker);

  right_blinker = gpio__construct_with_function(GPIO__PORT_0, 15, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_as_output(right_blinker);
}

void headlights_handler__turn_headlights_on(void) { gpio__reset(headlights); }

void headlights_handler__turn_headlights_off(void) { gpio__set(headlights); }

void headlights_handler__set_driver_steer_direction_and_move_speed(dbc_DRIVER_STEER_SPEED_s driver_steer_info) {
  driver_steer_direction = driver_steer_info.DRIVER_STEER_direction;
  driver_steer_move_speed = driver_steer_info.DRIVER_STEER_move_speed;
}

void headlights_handler__service_tail_lights(void) {
  if (driver_steer_move_speed <= 0) {
    // turn on
    gpio__reset(tail_lights);
  } else {
    // turn off
    gpio__set(tail_lights);
  }
}

void headlights_handler__service_blinkers(void) {
  if (destination_reached) {
    gpio__toggle(left_blinker);
    gpio__toggle(right_blinker);
  } else {
    if (driver_steer_direction < 0) {
      // L on, R off
      gpio__reset(left_blinker);
      gpio__set(right_blinker);
    } else if (driver_steer_direction > 0) {
      // R on, L off
      gpio__reset(right_blinker);
      gpio__set(left_blinker);
    } else {
      // turn all off
      gpio__set(left_blinker);
      gpio__set(right_blinker);
    }
  }
}